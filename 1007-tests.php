<?php
    session_start();

    $suser = file_get_contents("./admin.json");
    $decodeData = json_decode($suser, true);

    if ($_SESSION['password'] !== $decodeData['password']) {
        http_response_code(403);
        include("1007-403.php");
        exit;
    }

    $dir = "./tests";
    $scandir = scandir($dir);

    $count = 0;

    foreach ($scandir as $v) {

        if (strpos($v, ".json") !==false) {

            $count++;

        }
    }

    $count++;

    if (!empty($_FILES)) {

        if (array_key_exists("file", $_FILES)) {

            if (move_uploaded_file($_FILES["file"]["tmp_name"], "./tests/$count.json")) {

            } else {

                echo "Ошибка загрузки файла! <br>";

            }
        }
    }

?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>start</title>
</head>

<body>

    <form action="./1007-tests.php" method="post" enctype="multipart/form-data">
        <div><p>Выбирете файлы</p></div>
        <div>
            <input type="file" name="file">
        </div>
        <div>
            <input type="submit" value="отправить файл">
        </div>
    </form>

    <p><a href="1007-tests-list.php">Список тестов</a></p>
    <p><a href="1007-tests-logout.php">выйти</a></p>

</body>
</html>
<?php
    session_start();
    
    $suser = file_get_contents("./admin.json");
    $decodeData = json_decode($suser, true);

    $errors = [];

    $_POST['login'] = trim($_POST['login'], " \t\n\r\0\x0B");

    if ($_SESSION['user'] !== null) {
        header('location: 1007-tests-list.php');
        exit;
    }

    if (!empty($_POST)) {

        if ($_POST['password'] == null && $_POST['login'] !== "" && $_POST['login'] !== $decodeData['login']) {
            
            $_SESSION['user'] = $_POST['login'];
            $_SESSION['password'] = 1;
            header('location: 1007-tests-list.php');
            exit;

        } else {

            if ($_POST['login'] == $decodeData['login'] && $_POST['password'] == $decodeData['password']) {
                
                $_SESSION['user'] = $_POST['login'];
                $_SESSION['password'] = $_POST['password'];
                header('location: 1007-tests.php');
                exit;

            } elseif ($_POST['login'] !== null && $_POST['password'] !== null && $_POST['login'] !== $decodeData['login']) {

                $errors[] = 'форма заполнена не верно';

            } elseif ($_POST['login'] == $decodeData['login'] && $_POST['password'] !== $decodeData['password']) {

                $errors[] = 'неверный логин или пароль';

            }
        }
    }

?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>


<h1>Авторизация</h1>
<p>Введите логин и пароль для авторизации, или имя в поле "Логин" для прохождения теста.</p>

<? foreach ($errors as $error) : ?>
    <p><?= $error ?></p>
<? endforeach ?>

<form method="post" >
    <label for="lg">Логин</label>
    <input type="text" placeholder="Логин" name="login" id="lg">
    <label for="key">Пароль</label>
    <input type="text" placeholder="Пароль" name="password" id="key">
    <input type="submit" value="Войти">
</form>
    
</body>
</html>
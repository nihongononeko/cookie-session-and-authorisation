<?php
    session_start();

    if ($_SESSION['password'] == null) {

        http_response_code(403);
        include("1007-403.php");
        exit;

    }

    if (array_key_exists("test", $_GET)) {

        $dir = "./tests";
        $scandir = scandir($dir);

        $test = $_GET["test"];

        if (in_array($test, $scandir) == false) {

            http_response_code(404);
            include("1007-tests-404.php");
            // header("location: 1007-tests-404.php");
            exit;
        }
    }

?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">

    <title>start</title>
</head>
<body>

    <?php 

    if (isset($_GET["test"])) {

        foreach ($_GET as $testName) {

            if (file_get_contents("./tests/$testName") == true) {

                $json2 = file_get_contents("./tests/$testName");
            } 
        }

        if ($json2 !== false) {

            $decode_data = json_decode($json2, true);

            echo "<form action=\"1007-tests-go.php\" method=\"post\">";

            foreach ($decode_data as $ar) {

                foreach ($ar as $question => $answers) {

                    echo "<fieldset>";
                    echo "<legend> $question </legend>";

                    foreach ($answers as $key => $var) {

                        echo "<label><input type=\"checkbox\" name=\"$key\">$var</label>";

                    }

                    echo "</fieldset>";

                }
            }

            echo "<input type=\"submit\" value=\"Ответить\">";
            echo "</form>";

        } else {

            echo "запрашиваемого теста не существует";

        }
    }

    $right = $wrong = 0;
    if (!empty($_POST)) {

        foreach ($_POST as $k => $v1) {

            if ($k == "name") {

                continue;

            }
            if (gettype($k) == "integer") {

                $right++;

            } else {

                $wrong++;

            }
        }

        echo "правильных ответов $right. Неправильных ответов $wrong. <br>";

        $name = $_POST["name"];

        if ($right == count($_POST) && $wrong == 0 ) {

            echo "<a target=\"blank\" href=\"./1007-tests-sertificat.php?name=$name\">Получить сертификат</a> <br>";

        }

        echo "<a href=\"./1007-tests-list.php\">выбрать тест</a>";
        
    }

    ?>

</body>
</html>
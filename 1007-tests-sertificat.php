<?php
session_start();

if ($_SESSION['password'] == null) {
    
    http_response_code(403);
    include("1007-403.php");
    exit;
}

header('Content-type: image/jpeg');

$img = ImageCreateFromJPEG("./tests/sertifikat.jpg");

$color = imagecolorallocate($img, 0, 0, 0);

$font = './tests/arial.ttf';

$name = $_SESSION["user"];

imagettftext($img, 24, 0, 100, 300, $color, $font, $name);

imagettftext($img, 24, 0, 155, 350, $color, $font, "прошел тест");
 
imagejpeg($img, NULL, 100);
?>
<?php
    session_start();

    $suser = file_get_contents("./admin.json");
    $decodeData = json_decode($suser, true);

    if ($_SESSION['password'] == null) {

        http_response_code(403);
        include("1007-403.php");
        exit;

    }

    if (!empty($_GET)) {

        $deletingTest = $_GET['test'];
        unlink("./tests/$deletingTest");
        print_r($_GET);

    }

    // echo 'в $_SESSION ';
    // print_r($_SESSION);
    // echo "<br>";

?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">

    <title>start</title>
</head>
<body>

    <?php

    echo "<p>Вы ".$_SESSION['user']."</P>";

    echo "<p>Вы можете:</p>";

    if ($_SESSION['password'] == $decodeData['password']) {

        echo "<a href=\"./1007-tests.php\">Добавить тест</a><br>";

    }

    $dir = "./tests";
    $scandir2 = scandir($dir);

    echo "<table>";

    foreach ($scandir2 as $v) {
        echo "<tr>";
        if (strpos($v, ".json")) {

            echo "<td><a href=\"./1007-tests-go.php?test=$v\">выполнить тест $v </a><td>";

            if ($_SESSION['password'] == $decodeData['password']) {

                echo "<td><a href=\"./1007-tests-list.php?test=$v\">удалить тест</a></td>";

            }
        }

        echo "</tr>";

    }

    echo "</table>";

    ?>

    <a href="1007-tests-logout.php">выйти</a>

</body>
</html>